const app = require("express")();
const rcon = require("rcon");
const fs = require("fs");

if(process.argv.length < 4) {
  console.error("usage: node index.js <RCON port> <API port>");
  process.exit(1);
}
const [,, RCON_PORT, API_PORT] = process.argv;

const leaderboards = {
    deaths: "ftbutilities:deaths",
    time: "ftbutilities:time_played",
};

app.param("leaderboard", (req, res, next, name) => {
    if(!leaderboards[name])
        res.send({leaderboards: Object.keys(leaderboards)});
    else {
        req.leaderboard = leaderboards[name];
        next();
    }
});

app.get("/leaderboard/:leaderboard", (req, res, next) => {
    call("leaderboards " + req.leaderboard, str => {
      if(req.leaderboard == "ftbutilities:time_played") {
        var regex = /^#(?<order>\d+) (?<username>.+?): \[(?<hoursceil>\d+)h\] (?:(?<days>\d+)d )?(?:(?<hours>\d+):)?(?<minutes>\d+):(?<seconds>\d+)$/;
        var lines = str.split("\n");
        lines.shift();
        res.send(lines.map(line => line.match(regex).groups));
      } else {
        res.send(str);
      }
    });
});

app.get("/tps", (req, res, next) => {
    call("cofh tps", res.send.bind(res));
});

app.get("/dynmap/stats", (req, res, next) => {
    call("dynmap stats", res.send.bind(res));
});

const afkRegex = /^\[\d\d:\d\d:\d\d\] \[Server thread\/INFO\] \[FTB Utilities\]: ([^ ]+) is (now|no longer) AFK$/;

app.get("/afk", (req, res, next) => {
    call("list", (listStr) => {
        var lines = fs.readFileSync("../mcserver_archive/logs/latest.log", "utf8").split("\n");

        var afk = {};
        var online = listStr.split("\n")[1];
        online = online ? online.split(", ") : [];

        for(var username of online)
            afk[username] = false;

        for(var line of lines) {
            var match = line.match(afkRegex);
            if(match) {
                var [, username, state] = match;
                if(afk[username] === undefined) continue; // igbore offline
                afk[username] = state == "now";
            }
        }

        var count = {
            afk: 0,
            total: 0,
        };

        for(var username in afk) {
            count.total++;
            count.afk += afk[username];
        }

        count.notAfk = count.total - count.afk;

        res.send({
            ...count,
            players: afk,
        });
    });
});

var callQ = []; // [[command, cb]]
function call(command, cb) {
    callQ.push([command, cb]);
    console.log(`Added "${command}" to queue`);
    if(callQ.length === 1)
        runCall();
}

function runCall() {
    if(callQ.length === 0) return;
    var [command, callback] = callQ[0];
    console.log(`Running "${command}"`);
    conn.once('response', function(str) {
        console.log("Response: " + str);
        callQ.shift();
        callback(str);
        runCall();
    });
    conn.send(command);
}

var connFailed = 1;
var conn = new rcon('localhost', RCON_PORT, 'aP4EmvMpfX');
conn.on('auth', function() {
    console.log("RCON auth successful on port", RCON_PORT);
    app.listen(API_PORT);
    console.log("Listening on port", API_PORT);
    connFailed = 0;
}).on('error', function(err) {
    console.log("Error: " + err);
    console.log("Error occured, exitting with " + connFailed);
    process.exit(connFailed);
}).on('end', function() {
    console.log("Connection closed, exitting with " + connFailed);
    process.exit(connFailed);
});

console.log("Loaded");
conn.connect();
