#/bin/bash

git pull

timeout=5;
while true; do
  node . $* && timeout=5;
  echo Sleeping for ${timeout}s;
  sleep $timeout;
  timeout=$(($timeout * 2));
done
